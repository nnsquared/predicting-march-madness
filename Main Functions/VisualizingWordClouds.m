clc;
clear;

textData=extractFileText('TrainingData.txt');
cleanTextData = erasePunctuation(textData);
cleanDocuments = tokenizedDocument(cleanTextData);
cleanDocuments = removeWords(cleanDocuments,stopWords);
cleanDocuments = removeShortWords(cleanDocuments,2);
cleanDocuments = removeLongWords(cleanDocuments,15);
cleanDocuments = normalizeWords(cleanDocuments);
cleanBag = bagOfWords(cleanDocuments);
% cleanBag = removeInfrequentWords(cleanBag,2);
[cleanBag,idx] = removeEmptyDocuments(cleanBag);
rawDocuments = tokenizedDocument(textData);
rawBag = bagOfWords(rawDocuments);
figure
subplot(1,2,1)
wordcloud(rawBag);
title("Raw Data")
subplot(1,2,2)
wordcloud(cleanBag);
title("Clean Data")

%%

filename = "glove.6B.300d";
if exist(filename + '.mat', 'file') ~= 2
    emb = readWordEmbedding(filename + '.txt');
    save(filename + '.mat', 'emb', '-v7.3');
else
    load(filename + '.mat')
end
v_king = word2vec(emb,'king')';
whos v_king
%%
v_1 = word2vec(emb,'loss');
v_2 = word2vec(emb,'win');
v_3 = word2vec(emb,'life');
vec2word(emb, v_1 - v_2 +  v_3)