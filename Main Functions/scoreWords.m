function scores = scoreWords(obj,words)
%SCOREWORDS scores sentiment of words
vec = word2vec(obj.emb,words);          % word vectors
if size(vec,2) ~= obj.emb.Dimension     % check num cols
    vec =  vec';                        % transpose as needed
end
[~,scores,~] = predict(obj.mdl,vec);    % get class probabilities
scores = scores(:,2) - scores(:,1);     % positive scores - negative scores
end