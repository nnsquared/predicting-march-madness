clc;
% clear;
close all;

% First is the beginning year.
% Second is the year in which tournament is played.

[~,~,Base] = xlsread(char(strcat(strcat(First,'To',Second),{' '},'Basic.xlsx')));
[~,~,SchoolInd]=xlsread('SchoolIndex3');
[~,~,Advanced]=xlsread(char(strcat(strcat(First,'To',Second),{' '},'Advanced.xlsx')));
[~,~,RSDR]=xlsread(strcat(First,'_RegularData.xlsx'));
[~,~,NEW]=xlsread('TEAMSIMP.xlsx');
MatchTeamName=NEW(1:end,2);
MatchTeamID=[0;cell2mat(NEW(2:end,1))];
%% Matching the team names from Tournament Data
StartDate=[0;cell2mat(SchoolInd(2:end,3))];
EndDate=[0;cell2mat(SchoolInd(2:end,4))];
[m1,n1]=size(Base);
[m2,n2]=size(Advanced);
[m3,n3]=size(SchoolInd);
if m1~=m2
    error('BaseName and AdvancedName not same');
end
BaseName=Base(1:end,2);
AdvancedName=Advanced(1:end,2);
SchoolName=SchoolInd(1:end,2);
for i=2:m1
    if strcmp(BaseName{i},AdvancedName{i})~=1
        display(BaseName{i});
        display(AdvancedName{i});
        warning('BaseName and AdvancedName not same');
    end
end


%% Extracting information from Base and Advanced according to Team Data
BaseNew=cell(size(Base));
AdvancedNew=cell(size(Advanced));
BaseNew(1,:)=Base(1,:);
AdvancedNew(1,:)=Advanced(1,:);k=0;
ProblemInd=zeros(100,1);
for i=2:m3
    T1=SchoolName{i};
    for j=2:m2
        T2=AdvancedName{j};
        TF = contains(T2,T1);
        if TF==1 && strcmp(T1,T2)==1
            BaseNew(i,:)=Base(j,:);
            AdvancedNew(i,:)=Advanced(j,:);
            break;
        end
    end
    if TF==0
        disp(T1);
        k=k+1;
        ProblemInd(k)=i;
        warning('Team Not Found');
    end
    
end
ProblemInd(k+1:end)=[];

%% Load Conference Summary

[~,~,CS]=xlsread(strcat(Second,'ConfSum.xlsx'));
RegSeason=CS(:,12);
Tourn=CS(:,13);
[p1,p2]=size(CS);
k1=1; k2=1;
Creg=zeros(m3,1); Ctrn=zeros(m3,1);
Name=cell(m3,1); Name1=cell(m3,1);

for i=2:p1
    if isnan(RegSeason{i})==0
        C1 = strsplit(RegSeason{i},', ');
        Ind=0;
        for j1=1:length(C1)
             Ind = find(contains(SchoolName,C1(j1)));
             if Ind==0
                 error('not found');
             end
             for p=1:length(Ind)
                 if strcmp(SchoolName(Ind(p)),C1(j1))==1
                    Creg(Ind(p))=1;
                    Name{Ind(p)}=SchoolName{Ind(p)};
                    break;
                 end
             end
        end
    end
    if isnan(Tourn{i})==0
        C2 = strsplit(Tourn{i},', ');
        Ind=0;
        for j1=1:length(C2)
             Ind = find(contains(SchoolName,C2(j1)));
             if Ind==0
                 error('not found');
             end
             for p=1:length(Ind)
                 if strcmp(SchoolName(Ind(p)),C2(j1))==1
                    Ctrn(Ind(p))=1;
                    Name1{Ind(p)}=SchoolName{Ind(p)};
                    break;
                 end
             end
        end
    end
end

  

%% Extracting Relevant Features
TotalGamesPlayed=BaseNew(1:end,3);
TotalWins=BaseNew(1:end,4);
WLpercentage=BaseNew(1:end,6);
ConfWin=BaseNew(1:end,9);
ConfLoss=BaseNew(1:end,10);
TeamPoints=AdvancedNew(1:end,15);
TSper=AdvancedNew(1:end,21);
TOV=AdvancedNew(1:end,27);
AST=AdvancedNew(1:end,23);
Reb=AdvancedNew(1:end,22);
Stl=AdvancedNew(1:end,24);
SOS=AdvancedNew(1:end,8);
SRS=AdvancedNew(1:end,7);
NCAA=SchoolInd(1:end,15);
NCW=SchoolInd(1:end,17);
TeamID=[0;cell2mat(SchoolInd(2:end,18))];
Conf=SchoolInd(1:end,19);
VecForm={'TotalWins', 'TeamPoints', 'TSper', 'TOV', 'AST',...
    'Reb', 'Stl', 'Creg', 'Ctrn', 'SOS', 'SRS', 'NCAA', 'NCW','Big6' };
TotalCols=length(VecForm);
CV=zeros(m3,TotalCols);
for i=2:m3
    tf = ismember(Conf{i}, {'Big 12', 'Big Ten', 'SEC', 'ACC', 'Pac-12', 'Big East'});
     if ismember(i,ProblemInd)==0
        CV(i,:)=[TotalWins{i} TeamPoints{i}/TotalGamesPlayed{i} TSper{i} TOV{i} AST{i}...
            Reb{i} Stl{i} Creg(i) Ctrn(i) SOS{i} SRS{i} NCAA{i} NCW{i} tf];
    else
        CV(i,:)=[];
    end
end

%% The data is now created. Regular Season Data is also available.
% Start Creating Vectors for individual games

[m,n]=size(RSDR);
WteamID=[0;cell2mat(RSDR(2:end,3))];
LteamID=[0;cell2mat(RSDR(2:end,5))];
%%
Comp=zeros(m-1,TotalCols);
Y=zeros(m-1,1);
r=zeros(m-1,1);
for i=2:m
    
    W=find(TeamID==WteamID(i));
    L=find(TeamID==LteamID(i));
     if isempty(W)==1 
         jj=[];
         warning('Winning team not present');
         jj=find(MatchTeamID==WteamID(i));
         display(MatchTeamName{jj});
         continue;
     end
         
    
     if isempty(L)==1 
         jj=[];
         warning('Losing team not present');
         jj=find(MatchTeamID==LteamID(i));
         display(MatchTeamName{jj});
         continue;
     end
    if ismember(W,ProblemInd) || ismember(L,ProblemInd) 
        disp(SchoolName(W));
        disp(SchoolName(L));
        if EndDate(W)==str2double(First) || EndDate(L)==str2double(First)
            disp('EndDate');
            continue;
        end
        warning('Problem Indices are coming');
        continue;
    end
    if isempty(W)==1 || isempty(L)==1
        disp(W);
        disp(L);
        error('WL');
    end
    r(i-1)=randi([0,1]);
    if r(i-1)==1
            Comp(i-1,:)=CV(W,:)-CV(L,:);
    else
            Comp(i-1,:)=CV(L,:)-CV(W,:);
    end
    Y(i-1)=r(i-1);
end
    
    
