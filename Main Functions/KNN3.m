clc;
[~,~,TCR]=xlsread(strcat(Second,'_TourneyData.xlsx'));

%% Create Queries

[m4,n4]=size(TCR);
WteamQ=[0;cell2mat(TCR(2:end,3))];
LteamQ=[0;cell2mat(TCR(2:end,5))];
Queries=zeros(m4-1,TotalCols);
s=zeros(m4-1,1);
WteamName=strings(m4,1);
LteamName=strings(m4,1);
for i=2:m4
    
    WQ=find(TeamID==WteamQ(i));
    LQ=find(TeamID==LteamQ(i));
    WteamName(i)=string(SchoolName(WQ));
    LteamName(i)=string(SchoolName(LQ));
    if ismember(WQ,ProblemInd) || ismember(LQ,ProblemInd) || isempty(WQ)==1 || isempty(LQ)==1
        error('Problem Indices are coming');
     end
    s(i-1)=randi([0,1]);
    
%     fprintf('==================================\n');
%     disp(SchoolName{WQ});
%     disp(SchoolName{LQ});
%     fprintf('==================================\n');
bl=3;
    if s(i-1)==1
            Queries(i-1,:)=CV(WQ,:)-CV(LQ,:);
            plot(Queries(i-1,bl)','r*');
            hold on
            
    else
            Queries(i-1,:)=CV(LQ,:)-CV(WQ,:);
            plot(Queries(i-1,bl)','b*');
            hold on
    end
  
  
end
Expected=s;

%% K-nearest Neighbours

Mdl1 = fitcknn(Comp,Y,'NumNeighbors',1);
[label1,score1,cost1] = predict(Mdl1,Queries);
l=Expected-label1;
ts=length(l);
pa1=(ts-nnz(l))/ts;
fprintf('KNN -- %0.4f\n',pa1);
%% SVM
% mdl2 = fitcsvm(Comp,Y);
% [label2,score2] = predict(mdl2,Queries);
% l2=Expected-label2;
% ts2=length(l2);
% fprintf('SVM -- %0.4f\n',(ts2-nnz(l2))/ts2);

%% Binomial Logistic Regression

% Y1=double(nominal(Y));
% Ex=double(nominal(Expected));
% [B1,dev,stats] = glmfit(Comp,Y1);
% label3 = glmval(B1,Queries,'probit');
% label4=zeros(size(label1));
% for i=1:length(label3)
%     if label3()>=0.5
%         label4(i)=1;
%     else
%         label4(i)=0;
%     end
% end
% 
% l4=Expected-label4;
% ts4=length(l4);
% pa4=(ts4-nnz(l4))/ts4;
% fprintf('LogReg -- %0.4f\n',pa4);
%% Multinomial Logistic Regression


Y1=double(nominal(Y));
Ex=double(nominal(Expected));
[B1,dev,stats] = mnrfit(Comp,Y1);
label3 = mnrval(B1,Queries);
label4=zeros(size(label1));
for i=1:length(label3)
    if label3(i,1)>=label3(i,2)
        label4(i)=1;
    else
        label4(i)=2;
    end
end

l4=Ex-label4;
ts4=length(l4);
pa4=(ts4-nnz(l4))/ts4;
fprintf('LogReg -- %0.4f\n',pa4);


%% LDA


Discrim={'linear' ,'quadratic' , 'diaglinear' , 'diagquadratic' , 'pseudolinear' , 'pseudoquadratic'};
for i=1:length(Discrim)
mdl3 = fitcdiscr(Comp,Y,'DiscrimType',Discrim{i});
[label5,node] = predict(mdl3,Queries);
l5=Expected-label5;
ts5=length(l5);
pa5=(ts5-nnz(l5))/ts5;
fprintf('%s -- %0.4f\n',Discrim{i},pa5);
% display(pa5);
end



%% Decision Tree

mdl4 = fitctree(Comp(1:100,:),Y(1:100));
[label6,node] = predict(mdl4,Queries);
l6=Expected-label6;
ts6=length(l6);
pa6=(ts6-nnz(l6))/ts6;
fprintf('Dec Tree -- %0.4f\n',pa6);

%% Ensemble of learners

mdl5 = fitcensemble(Comp,Y);
[label7,node] = predict(mdl5,Queries);
l7=Expected-label7;
ts7=length(l7);
pa7=(ts7-nnz(l7))/ts7;
fprintf('Ensemble -- %0.4f\n',pa7);
