
% 2016

scoreA=[0.05 0.7015;
        0.04 0.6418;
        0.03 0.6119;
        0.02 0.5672;
        0.01 0.5522;
        0.00 0.5224];
    
    scoreB=[0.00 0.4179;
            0.01 0.4478;
            0.02 0.4478;
            0.03 0.4925;
            0.04 0.5672;
            0.05 0.6119];
    
    figure
    plot(scoreA(:,1),scoreA(:,2),'-o','linewidth',2,'MarkerSize',7,'MarkerFaceColor','g');
    hold on
    plot(scoreB(:,1),scoreB(:,2),'-o','linewidth',2,'MarkerSize',7,'MarkerFaceColor','r');
