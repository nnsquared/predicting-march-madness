%%
clear;
clc;

filename='Book2.xlsx';
data = readtable(filename,'TextType','string');
idxEmpty = strlength(data.Narrative) == 0;
data(idxEmpty,:) = [];


%%

dataTrain=data;
textDataTrain=data.Narrative;
textDataTrain = erasePunctuation(textDataTrain);
textDataTrain = lower(textDataTrain);
documentsTrain = tokenizedDocument(textDataTrain);
embeddingDimension = 100;
embeddingEpochs = 50;

emb = trainWordEmbedding(documentsTrain, ...
    'Dimension',embeddingDimension, ...
    'NumEpochs',embeddingEpochs, ...
    'Verbose',0);

%%

save('myemb2.mat');

