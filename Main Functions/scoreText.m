function score = scoreText(obj,text)
%SCORETEXT scores sentiment of text
tokens = split(lower(erasePunctuation(text)));            % split text into tokens
scores = obj.scoreWords(tokens);        % get score for each token
score = mean(scores,'omitnan');         % average scores
end