clc;
clear;



%% Load the file embedding
filename = "glove.6B.300d";
% load(filename + '.mat')
load('myemb2.mat');

%% Load word lexicons
pos = load_lexicon('positive-words.txt');
neg = load_lexicon('negative-words.txt');
%% Drop words not in the embedding
pos = pos(ismember(pos,emb.Vocabulary));
neg = neg(ismember(neg,emb.Vocabulary));

%% Get corresponding word vectors
v_pos = word2vec(emb,pos);
v_neg = word2vec(emb,neg);

%% Initialize the table and add the data
data = table;
data.word = [pos;neg];
pred = [v_pos;v_neg];
data = [data array2table(pred)];
data.resp = zeros(height(data),1);
data.resp(1:length(pos)) = 1;

%% Preview the table
head(data(:,[1,end,2:8 ]))

%% Divide data into training and testing
rng('default') % for reproducibility
c = cvpartition(data.resp,'Holdout',0.2); % 20% hold for testing
train = data(training(c),2:end);
Xtest = data(test(c),2:end-1);
Ytest = data.resp(test(c));
Ltest = data(test(c),1);
Ltest.label = Ytest;

%% Train
mdl = fitcdiscr(train,'resp');
% Predict on test data
Ypred = predict(mdl,Xtest);
cf = confusionmat(Ytest,Ypred);
% Display results
figure
vals = {'Negative','Positive'};
heatmap(vals,vals,cf);
xlabel('Predicted Label')
ylabel('True Label')
title({'Confusion Matrix of Linear Discriminant'; ...
    sprintf('Classification Accuracy %.1f%%', ...
    sum(cf(logical(eye(2))))/sum(sum(cf))*100)})

%% Display example test scoring
sent = sentiment(emb,mdl);
Ltest.score = sent.scoreWords(Ltest.word);
Ltest.eval = Ltest.score > 0 == Ltest.label;
disp(Ltest(randsample(height(Ltest),10),:))

%%
dbtype sentiment.m 28:33

%%
save('Main3.mat')

%%

% [sent.scoreText('Hampton may not win a game in this years tournament, but news of their bid did aggravate every Iowa State fans long-dormant 2001 which is a victory unto itself.') ...
% sent.scoreText('Before you poke at the lowly SWAC, or make a glib comment about Mike Davis, its worth noting the Tigers won at Kansas State and Michigan State this season') ...
% sent.scoreText('For one of the countrys 25 most accurate 3-point shooting teams, the Huskies dont shoot it very often: Just 30.8 percent of their field goal attempts this season were 3s')]

[sent.scoreText('Team A played Fantastically')...
sent.scoreText('Team A played okay')...
sent.scoreText('Team A did not play well')]

