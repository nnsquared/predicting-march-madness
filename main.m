%%
clear;

First='2015';
Second='2016';


iternew=1;
    MainVec=zeros(iternew,5);
    for inew=1:iternew
        run('ReadData3.m');
        run('KNN3.m');
        MainVec(inew,:)=[pa1 pa4 pa5 pa6 pa7];
        
    end
    disp(MainVec);

%%
clc;

if iternew > 1
    MainVec2=sum(MainVec)/iternew;
elseif iternew==1
    MainVec2=MainVec;
end
fprintf('%4s \t\t %4s \t\t %4s \t\t %4s \t\t %4s\n','KNN','LogReg','LDA','Dec Tree','Ensemble');
fprintf('%4.4f \t\t %4.4f \t\t %4.4f \t\t %4.4f \t\t %4.4f\n',MainVec2(1),MainVec2(2),MainVec2(3),MainVec2(4),MainVec2(5));

%% Compare Names
[~,~,article] = xlsread('2016Articles');
ArticleName=string(article(:,1));
SchoolNameS=string(SchoolName);
ba=length(BaseName);
for i=2:length(ArticleName)
    F1=ArticleName(i);
    t1=0;
    for j=2:ba
        [t1,p]=ismember(F1,SchoolNameS);
        if t1==1 
           if strcmp(SchoolNameS(p),F1)==1
               break;
           else
               disp(SchoolNameS(p));
           end
        end
    end
    
    if t1==0
        disp(F1);
    end
end
ArticleName=string(ArticleName);


PR538=[0;cell2mat(article(2:end,2))];
UPI=string(article(:,3));
RevSeed=[0;cell2mat(article(2:end,4))];
desert=string(article(:,5));
bleacher=string(article(:,6));
espn=string(article(:,7));
espnsc=[0;cell2mat(article(2:end,8))];
choose=[0;cell2mat(article(2:end,9))];
[ma,na]=size(article);
%%
load('Main.mat');
StringNew=strings(ma,4);
NumNew=zeros(ma,4);
Norma1=[100 68 6 max(choose)];
for i=2:ma
    
    StringNew(i,:)=[UPI(i) desert(i) bleacher(i) espn(i)];
    NumNew(i,:)=[PR538(i)/Norma1(1) RevSeed(i)/Norma1(2) espnsc(i)/Norma1(3) choose(i)/Norma1(4)];
    
end
Scores=zeros(ma,4);
Text=strings;
k=2;
for i=2:ma
    
    for j=1:4
        textData=StringNew(i,j);
        cleanTextData = erasePunctuation(textData);
        cleanTextData=normalizeWords(cleanTextData);
        Text1 = lower(cleanTextData);
        cleanTextData=Text1;
        
%       cleanDocuments = tokenizedDocument(cleanTextData);
%       cleanDocuments = removeWords(cleanDocuments,stopWords);
%       cleanDocuments = normalizeWords(cleanDocuments);
%       cleanBag = bagOfWords(cleanDocuments);
%       [cleanBag,idx] = removeEmptyDocuments(cleanBag);
        Scores(i,j)=sent.scoreText(cleanTextData);
         Score(k)=Scores(i,j);
         Text(k)=cleanTextData;
         k=k+1;
%         if Scores(i,j)<0
%             disp('========================');
%             disp(cleanTextData);
%             disp('========================');
%         end
        
    end
end
xlswrite('NewText.xlsx',[Score' Text']);

NewScore=sum(Scores,2)/4;
NewNum=sum(NumNew,2)/4;

WinInd=zeros(length(WteamName),1);
LossInd=zeros(length(WteamName),1);
%%
for i=2:length(WteamName)
    
    [ta,ind1]=ismember(WteamName(i),ArticleName);
    if ta==0
        WteamName(i)
        error('notfoundA');
    else
        WinInd(i)=ind1;
    end
    
    [tb,ind2]=ismember(LteamName(i),ArticleName);
    if tb==0
        LteamName(i)
        error('notfoundB');
    else
        LossInd(i)=ind2;
    end
end
    

for i=2:length(WteamName)
    
    if NewScore(WinInd(i))>=NewScore(LossInd(i)) || abs(NewScore(WinInd(i))-NewScore(LossInd(i))) < 0.05
        Score1(i)=1; 
    else 
        Score1(i)=0; 
    end
    if NewNum(WinInd(i))>=NewNum(LossInd(i))
        Score2(i)=1;
    else
        Score2(i)=0;
    end
    
    for j=1:4
        if Scores(WinInd(i),j) >= Scores(LossInd(i),j) || abs(Scores(WinInd(i),j)-Scores(LossInd(i),j) ) < 0.07
            ScoreNew(i,j)=1;
        else
            ScoreNew(i,j)=0;
        end
    end

    
end


    
display(sum(Score1(2:end))/67);
display(sum(Score2(2:end))/67);


S=sum(ScoreNew);
S=S/67;
fprintf('=========================================================\n')
fprintf('Maximum Prediction Rate\n');
fprintf('--------------------------\n');
fprintf('%4s \t | \t %4s \t | \t %4s \t | \t %4s\n','2013','2014','2015','2016');
fprintf('%4.2f \t | \t %4.2f \t | \t %4.2f \t | \t %4.2f\n',S(1),S(2),S(3),S(4));
fprintf('=========================================================\n')



fprintf('=========================================================\n')
fprintf('Average Prediction Rate For Expert Rankings\n');
fprintf('--------------------------------------------\n');
fprintf('%4s \t | \t %4s \t | \t %4s \t | \t %4s\n','2013','2014','2015','2016');
fprintf('%4.2f \t | \t %4.2f \t | \t %4.2f \t | \t %4.2f\n',NumNew(6,1),NumNew(7,1),NumNew(8,1),NumNew(9,1));
fprintf('=========================================================\n')


%%

    
    
    
    
    
    
    
    
    
