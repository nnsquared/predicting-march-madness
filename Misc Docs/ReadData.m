clc;
clear;
close all;

FirstString='2015To2016';
[~,~,BaseStats] = xlsread(char(strcat(FirstString,{' '},'Basic.xlsx')));
[~,~,SchoolInd]=xlsread('SchoolIndex');
[~,~,Advanced]=xlsread(char(strcat(FirstString,{' '},'Advanced.xlsx')));
[~,~,RSDR]=xlsread('RegularSeasonDetailedResults.xlsx');



[n,m]=size(BaseStats);

[~,h]=size(SchoolInd);
A5=cell(n,h);
A5(1,:)=SchoolInd(1,:);
Name=BaseStats(1:end,2);
AdvancedName=Advanced(1:end,2);
for i=2:n
    j=2;
    Fn3=Name{i};
    l=isstrprop(Fn3,'wspace');
    ll=length(l);
    if l(end)==1
        Fn=Fn3(1:ll-1);
    else
        Fn=Fn3;
    end
    Name{i}=Fn;
    
    if strcmp(Name{i},AdvancedName{i})==0
        error('Error in AdvancedName');
    end
          
    while 1
        Fn2=SchoolInd{j,2};
        TF = contains(Fn2,Fn);
        if TF==1
            A5(i,:)=SchoolInd(j,:);
            break;
        end
        j=j+1;
    end
    
end





TotalGamesPlayed=BaseStats(1:end,3);
TotalWins=BaseStats(1:end,4);
WLpercentage=BaseStats(1:end,6);
ConfWin=BaseStats(1:end,7);
ConfLoss=BaseStats(1:end,8);
TeamPoint=Advanced(1:end,15);
TSper=Advanced(1:end,21);
TOV=Advanced(1:end,27);
AST=Advanced(1:end,23);
Reb=Advanced(1:end,22);
Stl=Advanced(1:end,24);
Creg=A5(1:end,13);
Ctrn=A5(1:end,14);
SOS=Advanced(1:end,8);
SRS=Advanced(1:end,7);
NCAA=A5(1:end,15);
NCW=A5(1:end,17);
Extra1=Advanced(1:end,17);
Extra2=Advanced(1:end,18);
Extra3=Advanced(1:end,19);
Extra5=Advanced(1:end,20);
Extra6=Advanced(1:end,28);
Extra7=Advanced(1:end,26);
Extra8=Advanced(1:end,25);

TotalCols=20;

CV=zeros(n,TotalCols);



for i=1
        First={TotalWins{i} TeamPoint{i} TSper{i} TOV{i} AST{i}...
    Reb{i} Stl{i} Creg{i} Ctrn{i} SOS{i} SRS{i} NCAA{i} NCW{i} tf };
    
end
for i=2:n
    tf = ismember(Conf{i,2}, {'Big 12', 'Big Ten', 'SEC', 'ACC', 'Pac-12', 'Big East'});
    
    CV(i,:)=[TotalWins{i} TeamPoint{i}/TotalGamesPlayed{i} TSper{i} TOV{i} AST{i}...
    Reb{i} Stl{i} Creg{i} Ctrn{i} SOS{i} SRS{i} NCAA{i} NCW{i} tf, Extra1{i} Extra2{i} Extra3{i} Extra5{i} Extra6{i} Extra7{i}];
end
     
disp(First);
%% Retrieve the names from old games and their ids

TeamNo=zeros(n,1); TeamName=cell(n,1);
for j=2:n
    TempName=Name{j};
for i=2:length(Teams)
      TF1 = contains(Teams{i,2},TempName);
      if TF1==1 && strcmp(TempName,Teams{i,2})==1 && sum(sum(TempName-Teams{i,2}))==0 
         TeamNo(j,1)=Teams{i,1};
         TeamName{j,1}=Teams{i,2};
         break;
      end
end
if TF1==0
     disp(TempName);
     error('error');
end
end

%% Form a list of all possible games and then form a subtracted vector for each game,
% and also if the team was home or not

% already made list of
 %TotalWins, TeamPoint, TeamScorePerGame, TurnOver percentage, Assists,...
 % Rebounds, Steals, Regular championship, tournament
 % champions,SOS,SRS,NCAA qualify, NCW qualify, in big conference.

% get current season games :
[l1,w1]=size(RSDR);
Flag=0;
for i=2:l1
    if RSDR{i,1}==2015 && Flag==0
        st=i;
        disp(st);
        Flag=1;
    end
    if RSDR{i,1}==2016 && Flag==1
        en=i-1;
        disp(en);
        break;
    end
    
end
% 
k=1;
WteamNo=zeros(en-st+1,1);
LteamNo=zeros(en-st+1,1);
LteamName=zeros(en-st+1,1);
WteamName=zeros(en-st+1,1);

 for i=st:en
     
      WteamNo(k)=RSDR{i,3};
      LteamNo(k)=RSDR{i,5};
     k=k+1;
 end

 
%% Check if team number and wteam are same
NV=zeros(length(WteamNo),TotalCols);
Y=5*ones(length(WteamNo),1);
for i=1:length(WteamNo)
    
    CV1=find(TeamNo==WteamNo(i));
    for j=2:length(Teams)
          if Teams{j,1}==WteamNo(i)
              if (sum(sum(Teams{j,2}-TeamName{CV1})))~=0
                  error('error1');
              end
          end
    end
 
    CV2=find(TeamNo==LteamNo(i));
    for j=2:length(Teams)
          if Teams{j,1}==LteamNo(i)
              if (sum(sum(Teams{j,2}-TeamName{CV2})))~=0
                  error('error2');
              end
          end
    end
    
    choose=randi([1,2]);
    if choose==1
        NV(i,:)=CV(CV1,:)-CV(CV2,:);
        Y(i)=1;
    else
        NV(i,:)=CV(CV2,:)-CV(CV1,:);
        Y(i)=0;
    end

end

 save(strcat(FirstString,'CompleteData.mat'));


