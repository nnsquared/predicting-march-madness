% % Queries
% clear;
% clc;
% FirstString='2015To2016';
% load(strcat(FirstString,'CompleteData.mat'));
%%
[~,~,TCR]=xlsread('TourneyCompactResults.xlsx');
Flag=0;
for i=2:length(TCR)
    
    if TCR{i,1}==2016 && Flag==0
        st=i;
        Flag=1;
    end
    if TCR{i,1}==2017 && Flag==1
        en=i-1;
        break;
    end
end

%%
    


Queries=zeros(en-st+1,TotalCols);
k=1;
for i=st:en
    
    t1=find(TeamNo==TCR{i,3});
    t2=find(TeamNo==TCR{i,5});
    disp('==================================================');
    fprintf(' %s ----  %s\n',Name{t1},Name{t2});
    disp('==================================================');
    Queries(k,:)=CV(t1,:)-CV(t2,:);
    
    
    k=k+1;
end
    
mdl1 = fitcknn(NV,Y,'NumNeighbors',1);
[label1,score1,cost1] = predict(mdl1,Queries);

display(nnz(label1)/length(label1));
%  ,
% mdl2 = fitcsvm(NV,Y);
% [label2,score2] = predict(mdl2,Queries);
% display(nnz(label2)/length(label2));
    
