%%

 [~,~,RSDR]=xlsread('RegularSeasonDetailedResults.xlsx');

 CurrExcel(1,:)=RSDR(1,:);
 [m,n]=size(RSDR);
 year=zeros(m,1);
 for i=2:m
     
     year(i)=RSDR{i,1};
 end
    
 for y=2004:2017
     
     f=find(year==y);
     CurrExcel=RSDR(f(1):f(end),:);
     filename=strcat(num2str(y),'_RegularData.xlsx');
     xlswrite(filename,CurrExcel)
 end
     
 
 
