# Predicting March Madness

Comparing the average prediction rate of 1v1 matches in march madness from supervised learning algorithms applied to statistics vs sentiment analysis on expert opinion articles.

# Please read the wiki for this project before running the code. The wiki contains detailed explanation of the methodology used. 

Wiki can be accessed via the left sidebar or using the link below:

[Link to wiki](https://gitlab.com/nnsquared/predicting-march-madness/-/wikis/home)

# How to run the code

Add all the subfolders to the path in MATLAB and run main.m. Follow the instructions in wiki section to know more about the code.
